#!/usr/bin/env bash

function createConfiguration {
    local PROJECT_FILE=$1
    local PROJECT_NAME=$(basename --suffix .qgs $PROJECT_FILE)
    local URL="http://${OWS_HOST}:${OWS_PORT}/ows/?MAP=${PROJECT_NAME}/${PROJECT_NAME}&SERVICE=WMS&VERSION=1.1.0&REQUEST=GetCapabilities"

    echo "Processing $PROJECT_FILE file..."
    echo "Testing URL $URL"

    local URL_STATUS=$(curl -w '%{http_code}' --silent --fail --output /dev/null "${URL}")
    
    if [[ "${URL_STATUS}" = 200 ]] ; then
        
        echo "URL is accessible. Proceeding..."
        
        mapproxy-util autoconfig \
            --base /mapproxy/base.yaml \
            --capabilities $URL \
            --output ${PROJECT_NAME}/${PROJECT_NAME}.yaml \
            --output-seed ${PROJECT_NAME}/${PROJECT_NAME}.yaml.seed 
        
        echo "MapProxy configuration for ${PROJECT_NAME} created."
    else
        echo "URL is not accessible. Ommiting."
    fi
}

if [[ ! -z "${OWS_HOST}" ]] ; then
    echo "Variable OWS_HOST provided. Checking server..."
    ping -c 1 "${OWS_HOST}" > /dev/null && echo "${OWS_HOST} Online." || exit 1

    [[ -f /mapproxy/base.yaml ]] || cp /base.yaml /mapproxy/base.yaml # echo "File /mapproxy/base.yaml does not exist." &&
    
    echo "$(ls -1q */*.qgs | wc -l) project files was founded. Processing..."
    for project_file in */*.qgs; do
        createConfiguration $project_file
    done

    ln -s  */*.yaml* .
    
    if [ "${MULTIMAPPROXY}" = True ] ; then
        echo "Server setted to multiapp mode. Creating app."
        [[ -f /mapproxy/app.py ]] || cp /multi-app.py /mapproxy/app.py && echo "App already provided."
    else
        echo "Server setted to singleapp mode. Creating configuration."
        PROJECT_NAME=$(basename --suffix .yaml $(ls *.yaml | sort -n | head -1))
        mv ${PROJECT_NAME}.yaml /mapproxy/mapproxy.yaml
        rm ${PROJECT_NAME}.yaml.seed
    fi
else
    # create a MapProxy test configuration if none is provided
    echo "Variable OWS_HOST empty."
    [[ -f /mapproxy/mapproxy.yaml ]] || mapproxy-util create -t base-config /mapproxy && echo "Configuration already provided."
fi

[[ -f /mapproxy/app.py ]] || mapproxy-util create -t wsgi-app -f /mapproxy/mapproxy.yaml /mapproxy/app.py && echo "App already provided."

[[ -f /mapproxy/log.ini ]] || mapproxy-util create -t log-ini /mapproxy/log.ini && echo "Log configuration already provided."

chown -R www-data:www-data /projects
chmod -R g+rw /projects && chmod g+s /projects && cd /projects && umask 002 && cd /

chown -R www-data:www-data /mapproxy
chmod -R g+rw /mapproxy && chmod g+s /mapproxy && cd /mapproxy && umask 002

# execute MapProxy
[[ -f /mapproxy/uwsgi.ini ]] || exec /tini -- "$@"

exec /tini -- uwsgi --ini /mapproxy/uwsgi.ini
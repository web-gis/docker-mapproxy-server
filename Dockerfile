FROM python:3

LABEL Description="MapProxy uWSGI Server" Vendor="gpp" Version="1.0"

# ENV DEBIAN_FRONTEND=noninteractive
# ENV LC_ALL="C.UTF-8"
# ENV QGIS_DISABLE_MESSAGE_HOOKS=1
# ENV QGIS_NO_OVERRIDE_IMPORT=1
# ENV APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=1

ARG TINI_VERSION=v0.18.0
ARG git_repository=https://github.com/mapproxy/mapproxy.git

ENV MAPPROXY_VERSION 1.12a.0
ENV MAPPROXY_PROCESSES 4
ENV MAPPROXY_THREADS 2
ENV SERVER_TYPE http-socket

ENV MULTIMAPPROXY False
ENV ROOTDIR /projects

ADD https://github.com/krallin/tini/releases/download/$TINI_VERSION/tini /tini
RUN chmod +x /tini

RUN apt-get update && \
    apt-get install --no-install-recommends --no-install-suggests -y \
    build-essential \
    gcc \
    libgdal20 \
    libgdal-dev \
    libgeos-3.5.1 \
    libgeos-c1v5 \
    libgeos-dev \
    libjpeg-dev \
    libproj12 
#     libpython3.5 \
#     python3-dev \
#     python3-pip \
#     python3-setuptools \
#     python3-wheel \
#     python3-yaml \
#     zlib1g \
#     zlib1g-dev \

RUN pip install --no-cache-dir --prefer-binary \
    Pillow \
    Shapely \
    # PyYAML \
    uwsgi 

# Install server
RUN git clone --depth=1 $git_repository mapproxy && cd mapproxy && \
    python3 setup.py install && cd .. && \
    rm -rf mapproxy && \
    rm -rf /root/.cache /root/.ccache

# RUN apt-get purge -y --auto-remove \
#     build-essential \
#     libgdal-dev \
#     libgeos-dev \
#     libjpeg-dev \
#     python3-dev \
#     python3-pip \
#     python3-wheel \
#     zlib1g-dev \
#     && rm -rf /var/lib/apt/lists/*

RUN mkdir /mapproxy && mkdir /projects

VOLUME ["/mapproxy"]
VOLUME ["/projects"]

COPY mapproxy-docker-entrypoint.sh /docker-entrypoint.sh 
RUN chmod +x /docker-entrypoint.sh

COPY base.yaml /base.yaml
COPY multi-app.py /multi-app.py
RUN chmod +x multi-app.py

WORKDIR /projects

EXPOSE 8080
# Stats
EXPOSE 9191

ENTRYPOINT ["/docker-entrypoint.sh"]

# USER www-data

CMD uwsgi --$SERVER_TYPE 0.0.0.0:8080 --wsgi-file /mapproxy/app.py --uid www-data --gid www-data --processes $MAPPROXY_PROCESSES --threads $MAPPROXY_THREADS --enable-threads --wsgi-disable-file-wrapper --master --need-app --umask 002 --stats 0.0.0.0:9191 